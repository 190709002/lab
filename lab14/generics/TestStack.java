package generics;

import java.util.List;

public class TestStack {
    public static void main(String[] args){
        //testStack(new StackImpl<Integer>());

        testStack(new StackArrayListImpl<Number>());


    }
    public static void testStack(Stack<Number> stack){
        //StackArrayListImpl<Integer> st = (StackArrayListImpl) stack;
        stack.push(5);
        stack.push(6.5);
        stack.push(3000000000000L);
        stack.push(11);
        stack.push(23);

        System.out.println("stack 1: " +stack.toList());


        Stack<Integer> stack2 = new StackImpl<>();
        stack2.push(44);
        stack2.push(11);
        stack2.push(55);
        System.out.println("stack2 : " + stack2.toList());

        stack.addAll(stack2);
        System.out.println("stack 1: " +stack.toList());


        while (!stack.empty()){
            System.out.println(stack.pop());
        }

        System.out.println(stack.toList());


    }
}
