import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
		int attempts=1;

		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it: ");
		
		int guess = reader.nextInt(); //Read the user input

		while(number!=guess && guess!= -1) {
			System.out.println("Sorry!");
			if (number>guess){
				System.out.println("Mine is greater than your guess.");
			}else{
				System.out.println("Mine is smaller than your guess");
			}
			++attempts;

			System.out.println("Type -1 to quit or guess another:");
			guess = reader.nextInt();
		}
		if (guess==number){
			System.out.println("Congratilations");
			System.out.println("You won after "+ attempts + " times");
		}
		
		
		reader.close(); //Close the resource before exiting
	}

}